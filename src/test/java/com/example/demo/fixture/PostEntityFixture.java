package com.example.demo.fixture;

import com.example.demo.domain.UserRole;
import com.example.demo.domain.entity.PostEntity;
import com.example.demo.domain.entity.UserEntity;

import java.sql.Timestamp;
import java.time.Instant;

public class PostEntityFixture {
    public static PostEntity get(String userName, String password) {
        PostEntity postEntity = PostEntity.builder()
                .id(1)
                .user(UserEntityFixture.get(userName, password))
                .title("title")
                .body("body")
                .build();
        return postEntity;
    }
}
