package com.example.demo.service;

import com.example.demo.domain.dto.PostDto;
import com.example.demo.domain.entity.PostEntity;
import com.example.demo.domain.entity.UserEntity;
import com.example.demo.exception.AppException;
import com.example.demo.exception.ErrorCode;
import com.example.demo.fixture.PostEntityFixture;
import com.example.demo.fixture.TestInfoFixture;
import com.example.demo.fixture.UserEntityFixture;
import com.example.demo.repository.AlarmEntityRepository;
import com.example.demo.repository.CommentEntityRepository;
import com.example.demo.repository.LikeEntityRepository;
import com.example.demo.repository.PostRepository;
import com.example.demo.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
public class PostServiceTest {

    PostService postService;

    PostRepository postRepository = Mockito.mock(PostRepository.class);
    UserRepository userRepository = Mockito.mock(UserRepository.class);
    AlarmEntityRepository alarmEntityRepository = Mockito.mock(AlarmEntityRepository.class);
    LikeEntityRepository likeEntityRepository = Mockito.mock(LikeEntityRepository.class);
    CommentEntityRepository commentEntityRepository = Mockito.mock(CommentEntityRepository.class);


    @BeforeEach
    void setUp() {
        postService = new PostService(postRepository, userRepository, likeEntityRepository, commentEntityRepository, alarmEntityRepository);

    }


    @Test
    @DisplayName("조회 성공")
    void success_post_get() {
        TestInfoFixture.TestInfo fixture = TestInfoFixture.get();
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName(fixture.getUserName());

        PostEntity postEntity = PostEntityFixture.get(fixture.getUserName(), "1q2w3e4r");

        when(postRepository.findById(fixture.getPostId())).thenReturn(Optional.of(postEntity));

        PostDto postDto = postService.get(fixture.getPostId());
        assertEquals(fixture.getUserName(), postDto.getUserName());
    }

    @Test
    @DisplayName("등록 성공")
    void post_success() {
        TestInfoFixture.TestInfo fixture = TestInfoFixture.get();

        PostEntity mockPostEntity = mock(PostEntity.class);
        UserEntity mockUserEntity = mock(UserEntity.class);

        when(userRepository.findByUserName(fixture.getUserName()))
                .thenReturn(Optional.of(mockUserEntity));
        when(postRepository.save(any()))
                .thenReturn(mockPostEntity);

        Assertions.assertDoesNotThrow(() -> postService.write(fixture.getTitle(), fixture.getBody(), fixture.getUserName()));
    }
    //포스트생성시 유저가 존재하지 않을 때 에러
    @Test
    @DisplayName("등록 실패 : 유저 존재하지 않음")
    void post_fail_no_user() {
        TestInfoFixture.TestInfo fixture = TestInfoFixture.get();
        when(userRepository.findByUserName(fixture.getUserName())).thenReturn(Optional.empty());
        when(postRepository.save(any())).thenReturn(mock(PostEntity.class));
        AppException exception = Assertions.assertThrows(AppException.class, () -> postService.write(fixture.getUserName(), fixture.getTitle(), fixture.getBody()));

        assertEquals(ErrorCode.USERNAME_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    @DisplayName("수정 실패 : 작성자!=유저")
    void modify_fail_not_match() {
        TestInfoFixture.TestInfo fixture = TestInfoFixture.get();
        UserEntity userFixture = UserEntityFixture.get("user1", "password");
        UserEntity userFixture2 = UserEntityFixture.get("user2", "password2");

        // post를 작성한 user는 user1
        when(postRepository.findById(fixture.getPostId()))
                .thenReturn(Optional.of(PostEntity.of("title","body",userFixture)));

        // post를 삭제 시도한 user는 user2
        when(userRepository.findByUserName(any()))
                .thenReturn(Optional.of(UserEntity.of(userFixture2.getUserName(), userFixture2.getPassword())));

        AppException exception = Assertions.assertThrows(AppException.class, ()
                -> postService.modify(fixture.getUserName(), fixture.getPostId(), fixture.getTitle(), fixture.getBody()));

        assertEquals(ErrorCode.INVALID_PERMISSION, exception.getErrorCode());
    }
    //포스트 수정시 포스트 없을 때 에러
    @Test
    @DisplayName("수정 실패 : 포스트 존재하지 않음")
    @WithMockUser
    void post_fail_no_post() {
        TestInfoFixture.TestInfo fixture = TestInfoFixture.get();
        when(postRepository.findById(fixture.getPostId())).thenReturn(Optional.empty());
        AppException exception = Assertions.assertThrows(AppException.class, () ->
                postService.modify(String.valueOf(fixture.getUserId()), fixture.getPostId(), fixture.getTitle(), fixture.getBody()));
        assertEquals(ErrorCode.POST_NOT_FOUND, exception.getErrorCode());
    }

    //포스트 수정시 유저가 존재하지 않을 때 에러
    @Test
    @DisplayName("수정 실패 : 유저 존재하지 않음")
    void modify_fail_no_user() {
        TestInfoFixture.TestInfo fixture = TestInfoFixture.get();
        when(postRepository.findById(fixture.getPostId())).thenReturn(Optional.of(mock(PostEntity.class)));
        when(userRepository.findByUserName(fixture.getUserName())).thenReturn(Optional.empty());
        AppException exception = Assertions.assertThrows(AppException.class, () -> postService.modify(String.valueOf(fixture.getUserId()), fixture.getPostId(), fixture.getTitle(), fixture.getBody()));
        assertEquals(ErrorCode.USERNAME_NOT_FOUND, exception.getErrorCode());
    }

    //포스트 삭제시 포스트가 존재하지 않을 때 에러
    @Test
    @DisplayName("삭제 실패 : 포스트 존재하지 않음")
    void delete_fail_no_post() {
        TestInfoFixture.TestInfo fixture = TestInfoFixture.get();
        when(postRepository.findById(fixture.getPostId())).thenReturn(Optional.empty());
        AppException exception = Assertions.assertThrows(AppException.class, () -> postService.delete(fixture.getUserName(), fixture.getPostId()));
        assertEquals(ErrorCode.POST_NOT_FOUND, exception.getErrorCode());
    }

    //포스트 삭제시 유저가 존재하지 않을 때 에러
    @Test
    @DisplayName("삭제 실패 : 유저 존재하지 않음")
    void delet_fail_no_user() {
        TestInfoFixture.TestInfo fixture = TestInfoFixture.get();
        when(postRepository.findById(fixture.getPostId())).thenReturn(Optional.of(mock(PostEntity.class)));
        when(userRepository.findByUserName(fixture.getUserName())).thenReturn(Optional.empty());
        AppException exception = Assertions.assertThrows(AppException.class, () -> postService.delete(fixture.getUserName(), fixture.getPostId()));
        assertEquals(ErrorCode.USERNAME_NOT_FOUND, exception.getErrorCode());
    }

    //포스트 삭제시 작성자와 유저가 존재하지 않을 때 에러
    @Test
    @DisplayName("삭제 실패 : 작성자와 유저가 다름")
    void delete_fail_no_match() {
        PostEntity mockPostEntity = mock(PostEntity.class);
        UserEntity mockUserEntity = mock(UserEntity.class);

        TestInfoFixture.TestInfo fixture = TestInfoFixture.get();
        when(postRepository.findById(fixture.getPostId())).thenReturn(Optional.of(mockPostEntity));

        // 포스트를 지우려는 user는 user1
        when(userRepository.findByUserName(fixture.getUserName()))
                .thenReturn(Optional.of(mockUserEntity));

        // 포스트를 작성한 user는 valid_user_name
        when(mockPostEntity.getUser()).thenReturn(UserEntityFixture.get("valid_user_name", "1123"));
        AppException exception = Assertions.assertThrows(AppException.class, () -> {
            postService.delete(fixture.getUserName(), fixture.getPostId());
        });
        assertEquals(ErrorCode.INVALID_PERMISSION, exception.getErrorCode());
    }

    @Test
    @DisplayName("포스트 삭제 성공")
    void postDeleteSuccess() {
        PostEntity mockPostEntity = mock(PostEntity.class);
        UserEntity mockUserEntity = mock(UserEntity.class);

        TestInfoFixture.TestInfo fixture = TestInfoFixture.get();
        when(postRepository.findById(fixture.getPostId()))
                .thenReturn(Optional.of(mockPostEntity));
        when(userRepository.findByUserName(fixture.getUserName()))
                .thenReturn(Optional.of(mockUserEntity));
        when(mockPostEntity.getUser()).thenReturn(UserEntityFixture.get(fixture.getUserName(), "1234"));
        boolean b = postService.delete(fixture.getUserName(), fixture.getPostId());
        assertTrue(b);
    }

}
