package com.example.demo.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AlgorithmServiceTest {
    AlgorithmService algorithmService = new AlgorithmService();

    @Test
    void sumOfDigit() {
        assertEquals(21, algorithmService.sumOfDigit(687));
        assertEquals(6, algorithmService.sumOfDigit(123));
        assertEquals(0, algorithmService.sumOfDigit(0));
    }
}