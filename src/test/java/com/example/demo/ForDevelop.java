package com.example.demo;

import com.example.demo.domain.entity.LikeEntity;
import com.example.demo.domain.entity.PostEntity;
import com.example.demo.repository.LikeEntityRepository;
import com.example.demo.repository.PostRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class ForDevelop {
    @Autowired
    LikeEntityRepository likeRepository;

    @Autowired
    PostRepository postRepository;

    @Test
    @DisplayName("soft delete 테스트")
    void postSaveLikeDeletePost() {
        PostEntity postEntity = PostEntity.builder()
                .title("eee")
                .body("fff")
                .build();

        PostEntity savedPost = postRepository.save(postEntity);

        LikeEntity likeEntity = LikeEntity.builder()
                .post(savedPost)
                .build();

        // 좋아요 누르기
        LikeEntity savedLike = likeRepository.save(likeEntity);
        System.out.println(savedLike.getPost().getId());
        System.out.println(savedLike.getId());

        // like를 누르고 개수 세기
        System.out.printf("like누르고 개수 세기:%d\n", likeRepository.count());

        // 좋아요 지우기 by post_id
        likeRepository.deleteAllByPost(savedPost);
        System.out.printf("지우고 개수 세기:%d\n", likeRepository.count());
        Optional<LikeEntity> likeEntity1 = likeRepository.findById(savedLike.getId());
        System.out.println(likeEntity1.isEmpty());
        System.out.printf("deleted_at:%s\n", likeEntity1.get().getDeletedAt());
        postRepository.delete(savedPost);

    }
}
