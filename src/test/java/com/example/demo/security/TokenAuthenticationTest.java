package com.example.demo.security;


import com.example.demo.domain.UserRole;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import com.example.demo.utils.JwtTokenUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.nio.charset.StandardCharsets;
import java.util.Date;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest(value = {
        "jwt.token.secret=mutsa.hello.world.ee.ff"
})
@AutoConfigureMockMvc
public class TokenAuthenticationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    UserService userService;

    private String secretKey="mutsa.hello.world.ee.ff";
    //token 생성하는 메서드
    private String generateToken_1hour(String userName, UserRole role){
        //claims 생성
        return JwtTokenUtils.generateAccessToken(userName, secretKey, 1000 * 60 * 60);
    }
    private String generateToken_1msec(String userName, UserRole role){
        return JwtTokenUtils.generateAccessToken(userName, secretKey, 1);
    }

    @Test
    @Transactional
    @DisplayName("jwt 인증 성공")
    void authenticatedUser() throws Exception {

        // dummy user생성
        userService.join("authtestuser", "1q2w3e4r");

        // token 생성
        String token = generateToken_1hour("authtestuser", UserRole.USER);

        mockMvc.perform(get("/api/v1/hello/api-auth-test")
                    .with(csrf())
                    .header(HttpHeaders.AUTHORIZATION, "Bearer " + token))
                .andExpect(status().isOk())
                .andExpect(content().string("ok"))
                .andDo(print());
    }
    @Test
    @Transactional
    @DisplayName("jwt 인증 실패 - token을 보내지 않음") // Bearer가 없을 때
    void jwtAuthenticateFail() throws Exception {
        mockMvc.perform(get("/api/v1/auth-test-api")
                    .with(csrf())
                    .header(HttpHeaders.AUTHORIZATION, "Bearer ")) // Bearer가 없을 때
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.result.errorCode").value("INVALID_TOKEN"))
                .andDo(print());
    }
}
