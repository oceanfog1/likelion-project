package com.example.demo.security.filter;

import com.example.demo.domain.response.ErrorResponse;
import com.example.demo.domain.response.Response;
import com.example.demo.exception.ErrorCode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class ExceptionHandlerFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            filterChain.doFilter(request, response);
        } catch (PrematureJwtException e) {
            log.error("접근이 허용되지 않은 토큰입니다.");
        } catch (ExpiredJwtException e) {
            log.error("만료된 토큰입니다.");

        } catch (ArrayIndexOutOfBoundsException e) {
            log.error("Token이 없습니다.");
            ObjectMapper objectMapper = new ObjectMapper();
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setCharacterEncoding("UTF-8");
            objectMapper.writeValue(response.getWriter(),
                    Response.error("ERROR", new ErrorResponse(ErrorCode.INVALID_TOKEN, "")));
        } catch (Exception e) {

        }
    }

}
